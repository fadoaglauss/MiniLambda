#include "LexicalAnalysis.h"

LexicalAnalysis::LexicalAnalysis(const char* filename) : m_line(0) {
    m_file = fopen(filename, "r");
    if (!m_file){throw "Unable to open file";}
}
LexicalAnalysis::~LexicalAnalysis() { fclose(m_file); }
int LexicalAnalysis::line() const { return m_line; }

struct Lexeme LexicalAnalysis::nextToken() {
  struct Lexeme lex = {"", END_OF_FILE };
	int state = 1;
	while(state != 9 && state != 10){
    int c = getc(m_file);
    switch (state) {
      case 1:
        if(c == -1){state = 10;}
        else if(c == ' ' || c == '\t' || c == '\n' || c == '\r'){}
        else if(c == '#'){state = 2;}
        else if(isdigit(c)){lex.token += (char)c; state = 3;}
        else if(c == '!' || c == '='){lex.token += (char)c; state = 4;}
        else if(c == '<' || c == '>'){lex.token += (char)c; state = 5;}
        else if(c == '-'){lex.token += (char)c; state = 6;}
        else if(isalpha(c)){lex.token += (char) c; state = 7;}
        else if(c == '\"'){state = 8;}
        else if(c == ';' || c == ':' || c == '.' || c == ',' || c == '(' || c == ')'
                || c == '{' || c == '}' || c == '[' || c == ']' || c == '+' || c == '*'
                || c == '/' || c == '%'){lex.token += (char)c; state = 9;}
        else{
          lex.token += (char)c;
          lex.type = INVALID_TOKEN;
          state = 10;
        }
        break;
      case 2:
        if(c == '\n'){state = 1;}
        else if(c == -1){
          state = 10;
          //TODO: criar exeção de erro
          lex.type = UNEXPECTED_EOF;
        }
      case 3:
        if(isdigit(c)){lex.token += (char)c;}
        else{
          if(c != -1){ungetc(c, m_file);}
          state = 10;
          lex.type = NUMBER;
        }
        break;
      case 4:
				if(c == '='){lex.token += (char)c; state = 9;}
				else{
          //TODO: criar exeção de erro
					if(c == -1){lex.type = UNEXPECTED_EOF;}
          else{lex.type = INVALID_TOKEN;}
					state = 10;
				}
				break;
  		case 5:
				if(c == '='){lex.token += (char)c;}
        else{
          if(c != -1){ungetc(c, m_file);}
          lex.type = NUMBER;
        }
        state = 9;
				break;
  		case 6:
				if(c == '>'){lex.token += (char)c;}
        else{
					if(c != -1){ungetc(c, m_file);}
					lex.type = NUMBER;
				}
				state = 9;
				break;
  		case 7:
		    if(isalpha(c)){lex.token += (char)c;}
				else{
					if(c != -1){ungetc(c, m_file);}
					lex.type = STRING;
          state = 9;
        }
        break;
			case 8:
				if(c == '\"'){lex.type = STRING; state = 10;}
				else{
					if(c == -1){lex.type = UNEXPECTED_EOF; state = 10;}
					else{lex.token += (char)c;}
				}
				break;
		}
  }
	if(state == 9){
		if(m_symbols.contains(lex.token)){lex.type = m_symbols.find(lex.token);}
		else{lex.type = VAR;}
	}
  return lex;
}
