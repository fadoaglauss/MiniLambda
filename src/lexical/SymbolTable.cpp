#include "SymbolTable.h"

SymbolTable::SymbolTable() {
		m_st["."] = DOT;
		m_st[","] = COMMA;
		m_st[":"] = SEMI_COLON;
		m_st[";"] = DOT_COMMA;
		m_st["("] = PAR_OPEN;
		m_st[")"] = PAR_CLOSE;
		m_st["{"] = CBRA_OPEN;
		m_st["}"] = CBRA_CLOSE;
		m_st["["] = SBRA_OPEN;
		m_st["]"] = SBRA_CLOSE;
		m_st["->"] = ARROW;

		m_st["print"] = PRINT;
		m_st["println"] = PRINTLN;
		m_st["if"] = IF;
		m_st["else"] = ELSE;
		m_st["while"] = WHILE;
		m_st["load"] = LOAD;
		m_st["new"] = NEW;
		m_st["zero"] = ZERO;
		m_st["rand"] = RAND;
		m_st["fill"] = FILL;
		m_st["show"] = SHOW;
		m_st["sort"] = SORT;
		m_st["add"] = ADD;
		m_st["set"] = SET;
		m_st["filter"] = FILTER;
		m_st["remove"] = REMOVE;
		m_st["each"] = EACH;
		m_st["apply"] = APPLY;
		m_st["at"] = AT;
		m_st["size"] = SIZE;

		m_st["and"] = AND;
		m_st["or"] = OR;
		m_st["=="] = EQUAL;
		m_st["!="] = DIFF;
		m_st["<"] = LOWER;
		m_st[">"] = HIGHER;
		m_st["<="] = LOWER_EQ;
		m_st[">="] = HIGHER_EQ;
		m_st["+"] = PLUS;
		m_st["-"] = MINUS;
		m_st["*"] = MUL;
		m_st["/"] = DIV;
		m_st["%"] = MOD;
}

SymbolTable::~SymbolTable() {}
bool SymbolTable::contains(std::string token) {return m_st.find(token) != m_st.end();}
enum TokenType SymbolTable::find(std::string token) {return this->contains(token) ? m_st[token] : INVALID_TOKEN;}
