#ifndef TOKENTYPE_H
#define TOKENTYPE_H

enum TokenType {
    // special tokens
    INVALID_TOKEN  = -2,
    UNEXPECTED_EOF = -1,
    END_OF_FILE    = 0,

    // symbols
    DOT,        // . (1)
    COMMA,      // , (2)
    SEMI_COLON, // : (3)
    DOT_COMMA,  // ; (4)
    PAR_OPEN,   // ( (5)
    PAR_CLOSE,  // ) (6)
    CBRA_OPEN,  // { (7)
    CBRA_CLOSE, // } (8)
    SBRA_OPEN,  // [ (9)
    SBRA_CLOSE, // ] (10)
    ARROW,      // -> (11)

    // keywords
    PRINT,     // print   (12)
    PRINTLN,   // println (13)
    IF,        // if      (14)
    ELSE,      // else	  (15)
    WHILE,     // while	  (16)
    LOAD,      // load    (17)
    NEW,       // new     (18)
    ZERO,      // zero    (19)
    RAND,      // rand	  (20)
    FILL,      // fill	  (21)
    SHOW,      // show    (22)
    SORT,      // sort    (23)
    ADD,       // add	  (24)
    SET,       // set 	  (25)
    FILTER,    // filter  (26)
    REMOVE,    // remove  (27)
    EACH,      // each    (28)
    APPLY,     // apply   (29)
    AT,        // at      (30)
    SIZE,      // size    (31)

    // operators
    AND,       // and	(32)
    OR,        // or	(33)
    EQUAL,     // ==	(34)
    DIFF,      // !=	(35)
    LOWER,     // <	(36)
    HIGHER,    // >	(37)
    LOWER_EQ,  // <=	(38)
    HIGHER_EQ, // >=	(39)
    PLUS,      // +	(40)
    MINUS,     // -	(41)
    MUL,       // *	(42)
    DIV,       // /	(43)
    MOD,       // %	(44)

    // others
    VAR,          // variable	(45)
    STRING,       // string	(46)
    NUMBER        // number	(47)
};

#endif
