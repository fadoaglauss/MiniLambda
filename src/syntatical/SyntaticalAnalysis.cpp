#include <cstdio>
#include <iostream>
#include "SyntaticalAnalysis.h"
#include <model/Value/IntValue/ConstIntValue.h>
#include <model/Command/CommandBlock.h>
#include <model/Command/PrintCommand.h>
#include <model/Value/StringValue/ConstStringValue.h>

using namespace std;

SyntaticalAnalysis::SyntaticalAnalysis(LexicalAnalysis& lexical) :
  current(lexical.nextToken()), lex(lexical) {
}

SyntaticalAnalysis::~SyntaticalAnalysis() {
}

void SyntaticalAnalysis::showError(){
	std::cout << "Error" << std::endl;
  exit(1);
}

void SyntaticalAnalysis::matchToken(TokenType type){
	if(current.type == type){current = lex.nextToken();}
  else{showError();}
}

Command* SyntaticalAnalysis::init(){
	Command* cmds = procStatements();
	matchToken(END_OF_FILE);
  return cmds;
}

//<statements> ::= <cmd> { <cmd> }
Command* SyntaticalAnalysis::procStatements(){
  CommandBlock* cb = new CommandBlock;

	cb->addCommand(procCmd());
	while(current.type == PLUS || current.type == MINUS || current.type == NUMBER || current.type == LOAD || current.type == NEW || current.type == VAR || current.type == PAR_OPEN || current.type == PRINT || current.type == PRINTLN || current.type == IF || current.type == WHILE){
		cb->addCommand(procCmd());
	}

  return cb;
}
//  std::cout << "procCmd: " << current.type << std::endl;
Command* SyntaticalAnalysis::procCmd(){
  Command* c;
  if(current.type == PLUS || current.type == MINUS || current.type == NUMBER || current.type == LOAD || current.type == NEW || current.type == VAR || current.type == PAR_OPEN){
    c = procAssign();
  }else if(current.type == PRINT || current.type == PRINTLN){
    c = procPrint();
  }else if(current.type == IF){
    c = procIf();
  }else if(current.type == WHILE){
    c = procWhile();
  }else{
  //TODO: ERROR
    showError();
    return NULL;
  }
  return c;
}
//<assign> ::= <expr> [ ':' <var> { ',' <var> } ] ';'
void SyntaticalAnalysis::procAssign(){
	procExpr();
	if(current.type == VAR){
		matchToken(SEMI_COLON);
		procVar();
		while(current.type == VAR){
			matchToken(COMMA);
			procVar();
		}
	}
	if(current.type == DOT_COMMA){
		matchToken(DOT_COMMA);
	}else{
		showError();
	}
}
//<print> ::= (print | println) '(' <text> ')' ';'
PrintCommand* SyntaticalAnalysis::procPrint(){
  int line = lex.line();
  bool newLine = false;
	switch(current.type){
		case PRINT:
        matchToken(PRINT);
        newLine = false;
        break;
		case PRINTLN:
        matchToken(PRINTLN);
        newLine = true;
        break;
		default:
      showError();
      break;
	}
	matchToken(PAR_OPEN);
	Type* value = procText();
	matchToken(PAR_CLOSE);
	matchToken(DOT_COMMA);

  PrintCommand* pc = new PrintCommand(value, newLine, line);
  return pc;
}
//<if> ::= if <bollexpr> '{' <statements> '}' [else '{' <statements> '}']
void SyntaticalAnalysis::procIf(){
	matchToken(IF);
	procBoolExpr();
	matchToken(CBRA_OPEN);
	procStatements();
	matchToken(CBRA_CLOSE);
	if(current.type == ELSE){
		matchToken(ELSE);
		matchToken(CBRA_OPEN);
		procStatements();
		matchToken(CBRA_CLOSE);
	}
}
//<while> ::= while <boolexpr> '{' <statements> '}'
void SyntaticalAnalysis::procWhile(){
	matchToken(WHILE);
	procBoolExpr();
	matchToken(CBRA_OPEN);
	procStatements();
	matchToken(CBRA_CLOSE);
}
//<text> ::= (<string> | <expr>) { ‘,’ (<string> | <expr>) }
Type* SyntaticalAnalysis::procText(){
	switch(current.type){
		case STRING: 	procString();	break;
		default:
        // FIXME: nao retornar aqui pq ainda tem coisas pra fazer
        return procExpr();
        break;
	}

	while(current.type == COMMA){
		matchToken(DOT);
		switch(current.type){
			case STRING: 	procString();	break;
			default:		procExpr(); 	break;
		}
	}
}
//<boolexpr> ::= <expr> <boolop> <expr> { (and | or) <boolexpr> }
void SyntaticalAnalysis::procBoolExpr(){
	procExpr();
	procBoolop();
	procExpr();
	while(current.type == AND || current.type == OR){
		switch(current.type){
			case AND: 	matchToken(AND);	break;
			case OR:	matchToken(OR);		break;
			default:	showError(); 		break;
		}
		procBoolExpr();
	}
}
//<boolop> ::= '==' | '!=' | '<' | '>' | '<=' | '>='
void SyntaticalAnalysis::procBoolop(){
	switch(current.type){
		case EQUAL:		matchToken(EQUAL);		break;
		case DIFF:		matchToken(DIFF);		break;
		case LOWER:		matchToken(LOWER); 		break;
		case HIGHER:	matchToken(HIGHER); 	break;
		case LOWER_EQ:	matchToken(LOWER_EQ);	break;
		case HIGHER_EQ:	matchToken(HIGHER_EQ);	break;
		default: 		showError(); 			break;
	}
}
//<expr> ::= <term> [ ('+' | '-') <term> ]
Type* SyntaticalAnalysis::procExpr(){
	Type* ret = procTerm();
	if(current.type == PLUS || current.type == MINUS){
		switch(current.type){
			case PLUS: 	matchToken(PLUS);		break;
			case MINUS:	matchToken(MINUS);		break;
			default:	showError(); 			break;
		}
		procTerm();
	}
  return ret;
}
//<term> ::= <factor> [ ('*' | '/' | '%') <factor> ]
Type* SyntaticalAnalysis::procTerm(){
	Type* ret = procFactor();
	if(current.type == MUL || current.type == DIV || current.type == MOD){
		switch(current.type){
			case MUL: 	matchToken(MUL);	break;
			case DIV:	matchToken(DIV);		break;
			case MOD: 	matchToken(MOD);	break;
			default:	showError(); 		break;
		}
		procFactor();
	}

  return ret;
}
//<factor> ::= [‘+’ | ‘-‘] <number> | <load> | <value> | '(' <expr> ')'
Type* SyntaticalAnalysis::procFactor(){
	switch(current.type){
		case PLUS:
		case MINUS:
		case NUMBER:
        if (current.type == PLUS)
            matchToken(PLUS);
        else if (current.type == MINUS)
            matchToken(MINUS);

        return procNumber();
		case LOAD:		procLoad();				break;
		case ZERO:
		case RAND:
		case FILL:
		case VAR:		procValue();			break;
		default: 		showError(); 			break;
	}
	matchToken(PAR_OPEN);
	procExpr();
	matchToken(PAR_CLOSE);
}
//<load> ::= load '(' <text> ')'
void SyntaticalAnalysis::procLoad(){
	matchToken(LOAD);
	matchToken(PAR_OPEN);
	procText();
	matchToken(PAR_CLOSE);
}
//<value> ::= (<new> | <var>) { '.' <array> } [ . <int> ]
void SyntaticalAnalysis::procValue(){
	switch(current.type){
		case NEW: 		procNew();	break;
		default: 		procVar();	break;
	}
	while(current.type == DOT){
		matchToken(DOT);
		if(current.type == AT || current.type == SIZE){
			procInt();
			break;
		}else{
			procArray();
		}
	}

}
//<new> ::= new (<nzero> | <nrand> | <nfill>)
void SyntaticalAnalysis::procNew(){
	matchToken(NEW);
	switch(current.type){
		case ZERO: 		procNZero();		break;
		case RAND: 		procNRand();		break;
		case FILL: 		procNFill();		break;
		default:		showError();	 	break;
	}
}
//<nzero> ::= zero '[' <expr> ']'
void SyntaticalAnalysis::procNZero(){
	matchToken(ZERO);
	matchToken(SBRA_OPEN);
	procExpr();
	matchToken(SBRA_CLOSE);
}
//<nrand> ::= rand '[' <expr> ']'
void SyntaticalAnalysis::procNRand(){
	matchToken(RAND);
	matchToken(SBRA_OPEN);
	procExpr();
	matchToken(SBRA_CLOSE);
}
//<nfill> ::= fill '[' <expr> ',' <expr> ']'
void SyntaticalAnalysis::procNFill(){
	matchToken(FILL);
	matchToken(SBRA_OPEN);
	procExpr();
	matchToken(COMMA);
	procExpr();
	matchToken(SBRA_CLOSE);
}
//<array> ::= <show> | <sort> | <add> | <set> | <filter> | <remove> | <each> | <apply>
void SyntaticalAnalysis::procArray(){
	switch(current.type){
		case SHOW: 		procShow();		break;
		case SORT: 		procSort();		break;
		case ADD: 		procAdd();		break;
		case SET: 		procSet();		break;
		case FILTER: 	procFilter();	break;
		case REMOVE: 	procRemove();	break;
		case EACH:		procEach();		break;
		case APPLY: 	procApply();	break;
		default:		showError(); 	break;
	}
}
//<show> ::= show '(' ')'
void SyntaticalAnalysis::procShow(){
	matchToken(SHOW);
	matchToken(PAR_OPEN);
	matchToken(PAR_CLOSE);
}
//<sort> ::= sort '(' ')'
void SyntaticalAnalysis::procSort(){
	matchToken(SORT);
	matchToken(PAR_OPEN);
	matchToken(PAR_CLOSE);
}
//<add> ::= add '(' <expr> ')'
void SyntaticalAnalysis::procAdd(){
	matchToken(ADD);
	matchToken(PAR_OPEN);
	procExpr();
	matchToken(PAR_CLOSE);
}
//<set> ::= set '(' <expr> ',' <expr> ')'
void SyntaticalAnalysis::procSet(){
	matchToken(SET);
	matchToken(PAR_OPEN);
	procExpr();
	matchToken(COMMA);
	procExpr();
	matchToken(PAR_CLOSE);
}
//<filter> ::= filter '(' <var> '->' <boolexpr> ')'
void SyntaticalAnalysis::procFilter(){
	matchToken(FILTER);
	matchToken(PAR_OPEN);
	matchToken(VAR);
	matchToken(ARROW);
	procBoolExpr();
	matchToken(PAR_CLOSE);
}
//<remove> ::= remove '(' <var> '->' <boolexpr> ')'
void SyntaticalAnalysis::procRemove(){
	matchToken(REMOVE);
	matchToken(PAR_OPEN);
	matchToken(VAR);
	matchToken(ARROW);
	procBoolExpr();
	matchToken(PAR_CLOSE);
}
//<each> ::= each '(' <var> '->' <statements> ')'
void SyntaticalAnalysis::procEach(){
	matchToken(EACH);
	matchToken(PAR_OPEN);
	matchToken(VAR);
	matchToken(ARROW);
	procStatements();
	matchToken(PAR_CLOSE);
}
//<apply> ::= apply '(' <var> '->' <statements> ')'
void SyntaticalAnalysis::procApply(){
	matchToken(APPLY);
	matchToken(PAR_OPEN);
	matchToken(VAR);
	matchToken(ARROW);
	procStatements();
	matchToken(PAR_CLOSE);
  //<size> ::= size '(' ')'
}
//<int> ::= <at> | <size>
void SyntaticalAnalysis::procInt(){
	switch(current.type){
		case AT: 		procAt();		break;
		case SIZE: 		procSize();		break;
		default:		showError(); 	break;
	}
}
//<at> ::= at '(' <expr> ')'
void SyntaticalAnalysis::procAt(){
	matchToken(AT);
	matchToken(PAR_OPEN);
	procExpr();
	matchToken(PAR_CLOSE);
}
void SyntaticalAnalysis::procSize(){
	matchToken(SIZE);
	matchToken(PAR_OPEN);
	matchToken(PAR_CLOSE);
}

void SyntaticalAnalysis::procVar(){
	matchToken(VAR);
}

ConstIntValue* SyntaticalAnalysis::procNumber(){
  int line = lex.line();
  std::string tmp = current.token;
	matchToken(NUMBER);
  int n = std::stoi(tmp);

  ConstIntValue* civ = new ConstIntValue(n, line);
  return civ;
}

ConstStringValue* SyntaticalAnalysis::procString(){
  int line = lex.line();
  std::string text = current.token;
  matchToken(STRING);

  ConstStringValue* csv = new ConstStringValue(text, line);
  return csv;
}
