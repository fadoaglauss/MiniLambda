#ifndef SYNTATICAL_ANALYSIS_H
#define SYNTATICAL_ANALYSIS_H

#include <lexical/LexicalAnalysis.h>

class Type;
class ConstIntValue;
class Command;
class PrintCommand;
class ConstStringValue;

class SyntaticalAnalysis{
	public:
		SyntaticalAnalysis(LexicalAnalysis& lexical);
		virtual ~SyntaticalAnalysis();
		Command* init();

	private:
		Lexeme current;
		LexicalAnalysis& lex;
		void matchToken(enum TokenType type);
		void showError();
		Command* procStatements();
		Command* procCmd();
		void procAssign();
		PrintCommand* procPrint();
		void procIf();
		void procWhile();
		Type* procText();
		void procBoolExpr();
		void procBoolop();
		Type* procExpr();
		Type* procTerm();
		Type* procFactor();
		void procLoad();
		void procValue();
		void procNew();
		void procNZero();
		void procNRand();
		void procNFill();
		void procArray();
		void procShow();
		void procSort();
		void procAdd();
		void procSet();
		void procFilter();
		void procRemove();
		void procEach();
		void procApply();
		void procInt();
		void procAt();
		void procSize();
		void procVar();
		ConstIntValue* procNumber();
		ConstStringValue* procString();
};

#endif
