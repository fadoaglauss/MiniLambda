#include <iostream>
#include <lexical/LexicalAnalysis.h>
#include <syntatical/SyntaticalAnalysis.h>
#include <model/Command.h>

int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("Usage: %s [MiniLambda File]\n", argv[0]);
        return 1;
    }

    try {
        LexicalAnalysis l(argv[1]);
        SyntaticalAnalysis s(l);

        Command* c = s.init();
        c->execute();
    } catch(int e){

    }
    return 0;
}
