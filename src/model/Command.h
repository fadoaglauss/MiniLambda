#ifndef COMMAND_H
#define COMMAND_H

#include <model/Line.h>

class Command : public Line {
public:
  Command(int line) : Line(line) {}
  virtual ~Command() {}

  virtual void execute() = 0;
};

#endif
