#ifndef ARRAY_H
#define ARRAY_H

#include <algorithm>
#include <string>

class Array {
public:
  Array(int size);
  ~Array();
  
  int at(int index);
  int size();
  void show();
  void set(int index, int value);
  void sort();
  Array* add(int value);
  Array* add(Array* array);

  std::string toString();

private:
  int m_size;
  int m_array[];
};

#endif
