#ifndef ASSIGN_COMMAND_H
#define ASSIGN_COMMAND_H

#include <list>
#include <model/Type.h>
#include <model/Command.h>
#include <model/Value/Variable/Variable.h>

class AssignCommand:public Command{
public:
  AssignCommand(Type* value, int line);
  virtual ~AssignCommand();
  void addVariable(Variable var);
  void execute();

private:
  std::list<Variable> m_vars;
  Type* m_value;
};

#endif
