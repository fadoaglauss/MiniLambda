#include "CommandBlock.h"

CommandBlock::CommandBlock() : Command(-1) {
}

CommandBlock::~CommandBlock() {
    for (std::list<Command*>::iterator it = m_commands.begin(), ite = m_commands.end();
       it != ite; it++) {
         Command* c = *it;
         delete c;
    }
}

void CommandBlock::addCommand(Command* c) {
    m_commands.push_back(c);
}

void CommandBlock::execute() {
    for (std::list<Command*>::iterator it = m_commands.begin(), ite = m_commands.end();
       it != ite; it++) {
         Command* c = *it;
         c->execute();
    }
}
