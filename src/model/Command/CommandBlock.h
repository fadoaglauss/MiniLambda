#ifndef COMMAND_BLOCK_H
#define COMMAND_BLOCK_H

#include <list>
#include <model/Command.h>

class CommandBlock: public Command {
public:
  CommandBlock();
  virtual ~CommandBlock();

  void addCommand(Command* c);
  virtual void execute();

private:
  std::list<Command*> m_commands;
};

#endif
