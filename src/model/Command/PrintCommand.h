#ifndef PRINT_COMMAND_H
#define PRINT_COMMAND_H

#include <model/Command.h>

class Type;

class PrintCommand: public Command {
public:
  PrintCommand(Type* value, bool newLine, int line);
  virtual ~PrintCommand();

  virtual void execute();

private:
  Type* m_value;
  bool m_newLine;
};

#endif
