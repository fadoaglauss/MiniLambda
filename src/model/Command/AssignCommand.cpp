#include <model/Command/AssignCommand.h>
#include <model/Value/IntValue/ConstIntValue.h>
//#include <model/Value/ArrayValue/ConstArrayValue.h>
#include <model/Value/StringValue/ConstStringValue.h>


AssignCommand::AssignCommand(Type* value, int line)
  : Command(line), m_value(value){
}

AssignCommand::~AssignCommand(){
  m_vars.clear();
  delete m_value;
}

void AssignCommand::addVariable(Variable var){
  m_vars.push_back(var);
}

void AssignCommand::execute(){
/*
  Type* value = m_value->type() == Type::VarType ?
    ((Variable*)m_value)->value() : m_value;

  Type* newValue = NULL;
  if(value->type() == Type::IntType){
    IntValue* iv = (IntValue*) value;
    newValue = new ConstIntValue(iv.value(), -1);
  }else if(value->type() == Type::StringType){
    StringValue* sv = (StringValue*) value;
    newValue = new ConstStringValue(sv.value(), -1);
  }else if(value->type() == Type::ArrayType){
    ArrayValue av = (ArrayValue) value;
    newValue = new ConstArrayValue(av.value(), -1);
  }else{
    //TODO: Impossivel pela gramatica
  }

  for(Variable v : m_vars){
    v.setValue(newValue);
  }
*/
}
