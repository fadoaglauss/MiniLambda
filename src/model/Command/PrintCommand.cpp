#include <iostream>
#include <model/Type.h>
#include <model/Value.h>
#include <model/Command/PrintCommand.h>
#include <model/Value/IntValue/IntValue.h>
#include <model/Value/StringValue/StringValue.h>


PrintCommand::PrintCommand(Type* value, bool newLine, int line)
  : Command(line), m_value(value), m_newLine(newLine) {
}

PrintCommand::~PrintCommand() {
    delete m_value;
}

void PrintCommand::execute() {
  Type* value = m_value;
//  Type* value = m_value->type() == Type::VarType ?
//    ((Variable*) m_value)->value() : m_value;

  std::string text = "";
  if (value->type() == Type::IntType) {
    IntValue* iv = (IntValue*) value;
    int n = iv->value();
    text = std::to_string(n);
  } else if (value->type() == Type::StringType) {
    StringValue* sv = (StringValue*) value;
    text = sv->value();
/*/
  } else if (value->type() == Type::ArrayType) {
    ArrayValue* av = (ArrayValue*) value;
    Array* a = av->value();
    text = a->str();
    */
  } else {
    // FIXME: Erro de tipos
  }

  std::cout << text;
  if (m_newLine)
    std::cout << std::endl;
}
