#ifndef INT_VALUE_H
#define INT_VALUE_H

#include <model/Value.h>

class IntValue : public Value<int> {
public:
    IntValue(int line) : Value(Type::IntType, line) {}
    virtual ~IntValue() {}

    virtual int value() = 0;
};

#endif
