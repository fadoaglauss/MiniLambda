#ifndef ARRAY_INT_VALUE_H
#define ARRAY_INT_VALUE_H

#include <model/Line.h>
#include <model/Type.h>
#include <model/Value/IntValue/IntValue.h>

class ArrayIntValue : public IntValue{
protected:
  Type* m_array;

public:
  ArrayIntValue(Type* array, int line): IntValue(line), m_array(array) {}
  virtual ~ArrayIntValue(){}

  virtual int value() = 0;

};

#endif
