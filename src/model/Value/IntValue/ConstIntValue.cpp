#include <model/Value/IntValue/ConstIntValue.h>

ConstIntValue::ConstIntValue(int value, int line) :
    IntValue(line), m_value(value) {
}

ConstIntValue::~ConstIntValue() {
}

int ConstIntValue::value() {
  return m_value;
}
