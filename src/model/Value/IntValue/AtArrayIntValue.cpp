#include <model/Value/IntValue/AtArrayIntValue.h>
#include <model/Array.h>
#include <model/Type.h>

AtArrayIntValue::AtArrayIntValue(Type* array, Type* index, int line)
  : ArrayIntValue(array, line), m_index(index){
}

AtArrayIntValue::~AtArrayIntValue(){
  delete m_array;
  delete m_index;
}

int AtArrayIntValue::value(){
  Array m_array = (Array)m_array;
  int m_index = (int)m_index;
  return m_array.at(m_index);
}
