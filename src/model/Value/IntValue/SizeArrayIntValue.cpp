#include <model/Value/IntValue/SizeArrayIntValue.h>
#include <model/Type.h>
#include <model/Array.h>

SizeArrayIntValue::SizeArrayIntValue(Type* array, int line)
  : ArrayIntValue(array, line){
  }

SizeArrayIntValue::~SizeArrayIntValue(){
  delete m_array;
}

int SizeArrayIntValue::value(){
  return ((ArrayValue)array).size();
}
