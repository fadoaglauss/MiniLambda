#ifndef CONST_INT_VALUE_H
#define CONST_INT_VALUE_H

#include <model/Value/IntValue/IntValue.h>

class ConstIntValue : public IntValue {
public:
  ConstIntValue(int value, int line);
  virtual ~ConstIntValue();

  virtual int value();

private:
  int m_value;
};
#endif
