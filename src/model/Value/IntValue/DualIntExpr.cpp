#include <model/Value/IntValue/DualIntExpr.h>
#include <model/Type.h>

DualIntExpr::DualIntExpr(IntOp op, Type* left, Type* right, int line)
  : IntValue(line), m_op(op), m_left(left), m_right(right){
}

DualIntExpr::~DualIntExpr(){
  delete m_left;
  delete m_right;
}

int DualIntExpr::value(){
  int m_left = (int)m_left;
  int m_right = (int)m_right;

  switch (m_op) {
    case Add:
      return int(m_left + m_right);
    case Sub:
      return int(m_left - m_right);
    case Mul:
      return int(m_left * m_right);
    case Div:
      return int(m_left / m_right);
    case Mod:
      return int(m_left % m_right);
  }
}
