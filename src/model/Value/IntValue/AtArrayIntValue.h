#ifndef AT_ARRAY_INT_VALUE_H
#define AT_ARRAY_INT_VALUE_H

#include <model/Value/IntValue/ArrayIntValue.h>

class AtArrayIntValue : public ArrayIntValue {
public:
  AtArrayIntValue(Type* array, Type* index, int line);
  virtual ~AtArrayIntValue();

  int value();

private:
  Type* m_index;
};
#endif
