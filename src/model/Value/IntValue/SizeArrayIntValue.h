#ifndef SIZE_ARRAY_INT_VALUE_H
#define SIZE_ARRAY_INT_VALUE_H

#include <model/Value/IntValue/ArrayIntValue.h>
#include <model/Type.h>

class SizeArrayIntValue : public ArrayIntValue{
public:
  SizeArrayIntValue(Type* array, int line);
  virtual ~SizeArrayIntValue();

  int value();
};
#endif
