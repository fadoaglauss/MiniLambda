#ifndef DUAL_INT_EXPR_H
#define DUAL_INT_EXPR_H

#include <model/Value/IntValue/IntValue.h>
#include <model/Type.h>

enum IntOp {Add = 1, Sub, Mul, Div, Mod};

class DualIntExpr : public IntValue {
public:
  DualIntExpr(IntOp op, Type* left, Type* right, int line);
  virtual ~DualIntExpr();
  int value();

private:
  IntOp m_op;
  Type* m_left;
  Type* m_right;
};
#endif
