#ifndef ARRAY_VALUE_H
#define ARRAY_VALUE_H

#include <model/Array.h>
#include <model/Value.h>

class ArrayValue : public Value<Array>{
public:
  ArrayValue(int line) : Value(Type::ArrayType, line) {}
  virtual ~ArrayValue() {}

  virtual Array value() = 0;
};

#endif
