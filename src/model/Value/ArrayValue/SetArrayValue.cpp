#include <model/Array.h>
#include <model/Value/ArrayValue/SetArrayValue.h>

SetArrayValue::SetArrayValue(Type* array, Type* index, Type* value, int line)
  : ArrayValue(line), m_array(array), m_index(index), m_value(value){
}

SetArrayValue::~SetArrayValue(){
  delete m_array;
  delete m_index;
  delete m_value;
}

Array SetArrayValue::value(){
  Array a = new Array((int*)m_size);
  a.set((int*)m_index, (int*)m_value);

  return a;
}
