#ifndef FILL_ARRAY_VALUE_H
#define FILL_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class FillArrayValue : public ArrayValue{
public:
  FillArrayValue(Type* size, Type* value, int line);
  virtual ~FillArrayValue();

  Array value();

private:
  Type* m_size;
  Type* m_value;
};

#endif
