#include <model/Array.h>
#include <model/Value/ArrayValue/ShowArrayValue.h>

ShowArrayValue::ShowArrayValue(Type* array, int line)
  : ArrayValue(line), m_array(array){
}
ShowArrayValue::~ShowArrayValue(){
  delete m_array;
}

Array ShowArrayValue::value(){
  return 0;
}
