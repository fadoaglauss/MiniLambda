#ifndef RAND_ARRAY_VALUE_H
#define RAND_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class RandArrayValue : public ArrayValue{
public:
  RandArrayValue(Type* size, int line);
  virtual ~RandArrayValue();

  Array value();
private:
    Type* m_size;
};

#endif
