#ifndef ZERO_ARRAY_VALUE
#define ZERO_ARRAY_VALUE

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class ZeroArrayValue : public ArrayValue{
public:
  ZeroArrayValue(Type* size, int line);
  virtual ~ZeroArrayValue();

  Array value();

private:
  Type* m_size;
};

#endif
