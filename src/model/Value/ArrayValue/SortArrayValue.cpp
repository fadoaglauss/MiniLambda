#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/SortArrayValue.h>

SortArrayValue::SortArrayValue(Type* array, int line)
  : ArrayValue(line), m_array(array){
}

SortArrayValue::~SortArrayValue(){
  delete m_array;
}

Array SortArrayValue::value(){
    return *((Array*)m_array).sort();
}
