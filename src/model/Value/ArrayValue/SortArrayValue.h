#ifndef SORT_ARRAY_VALUE_H
#define SORT_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class SortArrayValue : public ArrayValue{
public:
  SortArrayValue(Type* array, int line);
  virtual ~SortArrayValue();

  Array value();

private:
  Type* m_array;
};

#endif
