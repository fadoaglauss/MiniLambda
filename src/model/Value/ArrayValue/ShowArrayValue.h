#ifndef SHOW_ARRAY_VALUE_H
#define SHOW_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class ShowArrayValue : public ArrayValue{
public:
  ShowArrayValue(Type* array, int line);
  virtual ~ShowArrayValue();

  Array value();

private:
  Type* m_array;
};

#endif
