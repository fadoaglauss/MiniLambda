#include <model/Array.h>
#include <model/Value/ArrayValue/ZeroArrayValue.h>

ZeroArrayValue::ZeroArrayValue(Type* size, int line)
  : ArrayValue(line), m_size(size){
}

ZeroArrayValue::~ZeroArrayValue(){
  delete m_size;
}

Array ZeroArrayValue::value(){
  Array a = new Array((int*)m_size);
  for (int i = 0; i < (int*)m_size; i++) {
    a[i] = 0;
  }
  return a;
}
