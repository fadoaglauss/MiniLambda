#ifndef REMOVE_ARRAY_VALUE_H
#define REMOVE_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/Variable/Variable.h>
#include <model/Value/BoolValue/BoolValue.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class RemoveArrayValue : public ArrayValue{
public:
  RemoveArrayValue(Type* array, Variable* var, BoolValue* cond, int line);
  virtual ~RemoveArrayValue();

  Array value();

private:
  Type* m_array;
  Variable* m_var;
  BoolValue* m_cond;
};

#endif
