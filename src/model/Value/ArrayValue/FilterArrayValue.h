#ifndef FILTER_ARRAY_VALUE_H
#define FILTER_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Variable/Variable.h>
#include <model/Value/BoolValue/BoolValue.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class FilterArrayValue : public ArrayValue {
public:
  FillArrayValue(Type* array, Variable* var, BoolValue* cond, int line);
  virtual ~FillArrayValue();

  Array value();

private:
  Type* m_array;
  Variable* m_var;
  BoolValue* m_cond;
};

#endif
