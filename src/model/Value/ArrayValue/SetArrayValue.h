#ifndef SET_ARRAY_VALUE_H
#define SET_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class SetArrayValue : public ArrayValue{
public:
    SetArrayValue(Type* array, Type* index, Type* value, int line);
    virtual ~SetArrayValue();

    Array value();

private:
  Type* m_array;
  Type* m_index;
  Type* m_value;
};

#endif
