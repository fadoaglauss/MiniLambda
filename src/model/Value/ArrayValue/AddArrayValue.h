#ifndef ADD_ARRAY_VALUE_H
#define ADD_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class AddArrayValue : public ArrayValue{
public:
  AddArrayValue(Type* array, Type* value, int line);
  virtual AddArrayValue();

  Array value();
private:
  Type* m_array;
  Type* m_value;
};

#endif
