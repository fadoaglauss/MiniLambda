#ifndef APPLY_ARRAY_VALUE_H
#define APPLY_ARRAY_VALUE_H

#include <model/Command.h>
#include <model/Value/ArrayValue.h>
#include <model/Value/BoolValue/BoolValue.h>
#include <model/Value/Variable/Variable.h>

class ApplyArrayValue : public ArrayValue {
public:
  ApplyArrayValue(Type* array, Variable var, Command* cmd);
  virtual ~ApplyArrayValue();

  Array value();

private:
      Type* m_array;
      Variable* m_var;
      BoolValue* m_cond;
};

#endif
