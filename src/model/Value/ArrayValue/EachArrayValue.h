#ifndef EACH_ARRAY_VALUE_H
#define EACH_ARRAY_VALUE_H

#include <model/Type.h>
#include <model/Array.h>
#include <model/Command.h>
#include <model/Value/Variable/Variable.h>
#include <model/Value/ArrayValue/ArrayValue.h>

class EachArrayValue : public ArrayValue{
public:
  EachArrayValue(Type* array, Variable var, Command* cmd, int line);
  virtual ~EachArrayValue();

  Array value();

private:
  Type* m_array;
  Variable m_var;
  Command* m_cmd;
};
#endif
