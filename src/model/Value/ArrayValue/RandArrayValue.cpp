#include <cstdlib>
#include <model/Array.h>
#include <model/Value/ArrayValue/RandArrayValue.h>

RandArrayValue::RandArrayValue(Type* size, int line)
  : ArrayValue(line), m_size(size){
}

RandArrayValue::~RandArrayValue(){
  delete m_size;
}

Array RandArrayValue::value(){
  Array a = new Array((int*)m_size);

  for (int i = 0; i < (int*)m_size; i++) {
    a.at(i) = rand() % 8000;
  }
  return a;
}
