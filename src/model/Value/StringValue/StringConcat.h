#ifndef STRING_CONCAT_H
#define STRING_CONCAT_H

#include <model/Value/StringValue/StringValue.h>
#include <model/Type.h>

class StringConcat : public StringValue {
public:
    StringConcat(Type* left, Type* right, int line);
    ~StringConcat();

    std::string value();

private:
  Type* m_left;
  Type* m_right;
};
#endif
