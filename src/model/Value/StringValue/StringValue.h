#ifndef STRING_VALUE_H
#define STRING_VALUE_H

#include <string>
#include <model/Value.h>

class StringValue: public Value<std::string> {
public:
    StringValue(int line) : Value(Type::StringType, line) {}
    virtual ~StringValue() {}

    virtual std::string value() = 0;
};
#endif
