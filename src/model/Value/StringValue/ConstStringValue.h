#ifndef CONST_STRING_VALUE_H
#define CONST_STRING_VALUE_H

#include <model/Value/StringValue/StringValue.h>
#include <string>

class ConstStringValue : public StringValue {
public:
  ConstStringValue(std::string value, int line);
  virtual ~ConstStringValue();

  std::string value();

private:
    std::string m_value;
};

#endif
