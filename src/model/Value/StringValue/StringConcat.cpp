#include <model/Value/StringValue/StringConcat.h>
#include <string.h>

StringConcat::StringConcat(Type* left, Type* right, int line)
  : StringValue(line), m_left(left), m_right(right){
}

StringConcat::~StringConcat(){
  delete m_left;
  delete m_right;
}

std::string StringConcat::value(){
  std::string m_left = (std::string)m_left;
  std::string m_right = (std::string)m_right;
  return m_left + m_right;
}
