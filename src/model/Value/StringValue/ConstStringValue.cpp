#include <model/Value/StringValue/ConstStringValue.h>

ConstStringValue::ConstStringValue(std::string value, int line)
  : StringValue(line), m_value(value){}

ConstStringValue::~ConstStringValue(){
}

std::string ConstStringValue::value() {
  return m_value;
}
