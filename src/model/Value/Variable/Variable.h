#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>
#include <model/Value.h>
#include <model/Type.h>

class Type;
//template<class T>
//class Variable : public Value<T>{
class Variable : public Value<Type*>{
public:
  Variable(std::string name) : Value(Type::VarType, -1){}

  std::string getName(){ return m_name; }
  void setName(Type* v){ m_value = v; }

  Type* value(){ return m_value; }

private:
  std::string m_name;
  Type* m_value;

};

#endif
