#ifndef BOOL_VALUE_H
#define BOOL_VALUE_H

#include <model/Value.h>

class BoolValue : public Value<bool>{
public:
  BoolValue(int line) : Value(Type::BoolType, line) {}
  virtual ~BoolValue() {}

  virtual bool value() = 0;
};
#endif
