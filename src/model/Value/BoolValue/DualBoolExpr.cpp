#include <model/Value/BoolValue/DualBoolExpr.h>

DualBoolExpr::DualBoolExpr(BoolOp op, BoolValue* left, BoolValue* right, int line)
  : BoolValue(line), m_op(op), m_left(left), m_right(right) {
  }

DualBoolExpr::~DualBoolExpr() {
  delete m_left;
  delete m_right;
}

bool DualBoolExpr::value(){
  switch (m_op) {
    case And:
      return (m_left && m_right);
    case Or:
      return (m_left || m_right);
  }
}
