#include <model/Type.h>
#include <model/Value/BoolValue/CompareBoolValue.h>

CompareBoolValue::CompareBoolValue(RealOp op, Type* left, Type* right, int line)
 : BoolValue(line), m_op(op), m_left(left), m_right(right){
}

CompareBoolValue::~CompareBoolValue(){
  delete m_left;
  delete m_right;
}

bool CompareBoolValue::value(){
  switch (m_op) {
    case Equal:
      return m_left == m_right ? true : false;
    case NotEqual:
      return m_left != m_right ? true : false;
    case LowerThan:
      return m_left < m_right ? true : false;
    case LowerEqual:
      return m_left <= m_right ? true : false;
    case GreaterThan:
      return m_left > m_right ? true : false;
    case GreaterEqual:
      return m_left >= m_right ? true : false;
  }
}
