#ifndef COMPARE_BOOL_VALUE_H
#define COMPARE_BOOL_VALUE_H

#include <model/Type.h>
#include <model/Value/BoolValue/BoolValue.h>

enum RealOp { Equal = 1, NotEqual, LowerThan, LowerEqual, GreaterThan, GreaterEqual };

class CompareBoolValue : public BoolValue{
public:
  CompareBoolValue(RealOp op, Type* left, Type* right, int line);
  virtual ~CompareBoolValue();

  bool value();

private:
  RealOp m_op;
  Type* m_left;
  Type* m_right;

};

#endif
