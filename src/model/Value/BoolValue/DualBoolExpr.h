#ifndef DUAL_BOOL_EXPR_H
#define DUAL_BOOL_EXPR_H

#include <model/Value/BoolValue/BoolValue.h>

enum BoolOp {And = 1, Or};

class DualBoolExpr : public BoolValue{
public:
  DualBoolExpr(BoolOp op, BoolValue* left, BoolValue* right, int line);
  virtual ~DualBoolExpr();

  bool value();

private:
  BoolOp m_op;
  BoolValue* m_left;
  BoolValue* m_right;
};

#endif
