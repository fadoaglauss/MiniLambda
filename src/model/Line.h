#ifndef LINE_H
#define LINE_H

class Line {
public:
    Line(int line) : m_line(line) {}
    virtual ~Line() {}

    int line() const { return m_line; }

private:
    int m_line;
};
#endif
