#ifndef VALUE_H
#define VALUE_H

#include <model/Type.h>
#include <model/Line.h>

template<class T>
class Value : public Type, public Line {
public:
    Value(enum Type::ValueType type, int line)
        : Type(type), Line(line) {}

    virtual ~Value() {}

    virtual T value() = 0;

};

#endif
