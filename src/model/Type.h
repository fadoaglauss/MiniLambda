#ifndef TYPE_H
#define TYPE_H

class Type {
public:
    enum ValueType { IntType, StringType, BoolType, ArrayType, VarType };

    Type(enum ValueType type) : m_type(type) {}
    virtual ~Type() {}

    enum ValueType type() const { return m_type; }
private:
    enum ValueType m_type;
};

#endif
